//
// Created by thorellou on 27/03/2020.
//

#include <math.h>

double sinusFunc(double a, double b, double x) {
    return sin(2 * M_PI * ((a * x) + b));
}

#include <emscripten/bind.h>

EMSCRIPTEN_BINDINGS(sinusFunc) {
        emscripten::function("sinusFunc", &sinusFunc);
}
